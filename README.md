# AC Motor Anomaly Detection

* Monitor AC motors and transmit data real time to cloud.
* Send alerts if the motor behaves against the stipulated behaviour.  

[Click to view project planning sheet](https://docs.google.com/spreadsheets/d/1sIYvidpug-R774geXJFRzngUECxCkyAJBLQiXXCS6BM/edit?usp=sharing)


## Shunya OS API
* Shunya OS contains API's for a wide variety of sensor
* Shunya interfaces will be used in this project
* Click [here](http://demo.shunyaos.org/components) for a list of all the API's available

## PreRequisties 
* Ubuntu 16.04
* Docker Engine
* Shunya OS docker installed.

## Installation Guide
* [Ubantu](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/installUbuntu)
* [ShunyaOS docker](https://hub.docker.com/r/shunyaos/shunya-armv7)
* [Docker](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/dockerBasics#docker-installation) 

## How to contribute?
* Fork the repository 
* Start working on the task mentioned as in the issue
* Once completed comment on that issue with your repo link for verification
* Once accepted,send a merge request 
* If changes are mentioned make the changes are submit again.
* Once merge request is accpeted move on to the next task.

## Gains 
* Experience of working on a industry demanded project
* Working in a professional enviornment 
* Networking with like minded people


## Maintainer 
* [@chirag.chinvar5](https://gitlab.com/chirag.chinvar5)

## Contributors
* [@syeshaswinichowdary123](https://gitlab.com/syeshaswinichowdary123)